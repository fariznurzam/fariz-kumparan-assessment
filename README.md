## Kumparan Assessment with Django Rest Framework

### Requirements
- Python 3 (mandatory)
- Django 2 or higher
- djangorestframework 3 or higher
- django-filter 1.1
- virtualenv 15 or higher
- autopep8 1.3 or higher

### Notes
- Make sure already install Python3 and virtualenv on your local
- If there is no python3 on your local, use python3 from generated virtual environment
- Make sure python bin or virtual environment bin insert in PATH 

### Local Setup Guide
- `git clone https://fariznurzam@bitbucket.org/fariznurzam/fariz-kumparan-assessment.git`
- `cd fariz-kumparan-assessment`
- `git checkout develop` (optional)
- `virtualenv -p /usr/local/bin/python3 venv`
- `source venv/bin/activate`
- `pip install -r requirement.txt`
- `sudo easy_install pip` or `apt install python-pip` if there is no pip installed in virtual env (optional)
- `cd kumparannews`
- `python3 manage.py makemigrations api` to generate database migration
- `python3 manage.py migrate` to do migration
- `python3 manage.py createsuperuser` to create super user
- `username = admin`
- `email = admin@admin.com`
- `password = admin.password`
- `password (once again) = admin.password`
- `python3 manage.py loaddata initial_data` to dump fixture data to database
- `python3 manage.py test` to run unit test
- `python3 manage.py runserver` to run application

### How to use it
- `127.0.0.1:8000/news/` open News page
- `127.0.0.1:8000/news/<id>` change <id> to id of News, open detail News page by id
- `127.0.0.1:8000/news/?status=<status>` change <status> to status index (0 = draft, 1 = deleted, 2 = publish), open News filtered by status
- `127.0.0.1:8000/news/?topics=<topic_name>` change <topic_name> to topic name (example News, Sepak Bola), open News filtered by topic name
- `127.0.0.1:8000/news/?q=<q>` change <q> to News title, status index, or topic name, open News filtered by title, status, or topic name
- `127.0.0.1:8000/topic/` open Topic page
- `127.0.0.1:8000/topic/<id>` change <id> to id of Topic, open detail Topic page by id
