from rest_framework import serializers
from .models import News, Topic


class NewsSerializer(serializers.ModelSerializer):

    author = serializers.ReadOnlyField(source='author.username')

    class Meta:
        model = News
        fields = (
            'id',
            'title',
            'content',
            'status',
            'author',
            'topics',
            'created_at',
            'updated_at')
        read_only_fields = ('created_at', 'updated_at')


class TopicSerializer(serializers.ModelSerializer):

    author = serializers.ReadOnlyField(source='author.username')

    class Meta:
        model = Topic
        fields = (
            'id',
            'name',
            'description',
            'author',
            'created_at',
            'updated_at')
        read_only_fields = ('created_at', 'updated_at')
