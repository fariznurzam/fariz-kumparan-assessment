from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns
from .views import CreateView, DetailsView, TopicCreateView, TopicDetailsView
from rest_framework.authtoken.views import obtain_auth_token

urlpatterns = {
    url(r'^auth/', include('rest_framework.urls', namespace='rest_framework')),
    url(r'^news/$', CreateView.as_view(), name="create"),
    url(r'^news/(?P<pk>[0-9]+)/$', DetailsView.as_view(), name="details"),
    url(r'^topic/$', TopicCreateView.as_view(), name="create-topic"),
    url(r'^topic/(?P<pk>[0-9]+)/$',
        TopicDetailsView.as_view(),
        name="details-topic"),
    url(r'^get-token/', obtain_auth_token, name="get-token"),
}

urlpatterns = format_suffix_patterns(urlpatterns)
