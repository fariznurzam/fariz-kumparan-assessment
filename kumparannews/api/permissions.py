from rest_framework.permissions import BasePermission
from .models import News, Topic


class IsAuthor(BasePermission):

    def has_object_permission(self, request, view, obj):
        if isinstance(obj, News):
            return obj.author == request.user
        elif isinstance(obj, Topic):
            return obj.author == request.user
        return obj.author == request.user
