from django.test import TestCase
from .models import News, Topic
from rest_framework.test import APIClient
from rest_framework import status
from django.urls import reverse
from django.contrib.auth.models import User
from django.utils.http import urlencode
import datetime


class ModelTestCase(TestCase):

    def setUp(self):
        user = User.objects.create_superuser(
            username="admin", email="admin@admin.com", password="admin.password")
        self.news_name = "Ferrari 2019 Red Special Edition"
        self.news_name_2 = "Lamborghini Avantador Juara Best Sport Car 2018"
        self.news_content = "ini konten yang sangat panjang"
        self.news_status_draft = "0"
        self.news_status_publish = "2"
        self.news = News(
            title=self.news_name,
            content=self.news_content,
            status=self.news_status_draft,
            author=user)
        self.news_kedua = News(
            title=self.news_name_2,
            content=self.news_content,
            status=self.news_status_publish,
            author=user)
        self.topic_name = "Mobil Eropa"
        self.topic_name_2 = "Ferrari"
        self.topic = Topic(name=self.topic_name, author=user)
        self.topic_kedua = Topic(name=self.topic_name_2, author=user)

    def test_model_create_news(self):
        old_count = News.objects.count()
        self.news.save()
        new_count = News.objects.count()
        self.assertNotEqual(old_count, new_count)

    def test_model_create_topic(self):
        old_count_topic = Topic.objects.count()
        self.topic.save()
        new_count_topic = Topic.objects.count()
        self.assertNotEqual(old_count_topic, new_count_topic)

    def test_model_delete_news(self):
        self.news.save()
        before_delete_count = News.objects.count()
        self.news.delete()
        after_delete_count = News.objects.count()
        self.assertNotEqual(before_delete_count, after_delete_count)

    def test_model_delete_topic(self):
        self.topic.save()
        before_delete_count = Topic.objects.count()
        self.topic.delete()
        after_delete_count = Topic.objects.count()
        self.assertNotEqual(before_delete_count, after_delete_count)

    def test_model_assign_topic_to_news(self):
        self.news.save()
        self.topic.save()
        self.news.topics.add(self.topic)
        self.assertEqual(self.news.topics.get().name, self.topic.name)

    def test_model_news_create_topic(self):
        user = User.objects.create_superuser(
            username="test", email="test@admin.com", password="test.password")
        topic = "Motor Amerika"
        self.news.save()
        self.news.topics.create(name=topic, author=user)
        self.assertEqual(self.news.topics.get().name, topic)

    def test_model_assign_topic_to_news_expect_saved_by_topic(self):
        self.news.save()
        self.topic.save()
        self.news.topics.add(self.topic)
        self.assertEqual(self.topic.news_set.get().title, self.news.title)

    def test_model_filter_news_by_title(self):
        self.news.save()
        self.news_kedua.save()
        filtered_title = News.objects.filter(
            title__startswith="Ferrari").get().title
        self.assertEqual(self.news_name, filtered_title)

    def test_model_filter_news_by_status(self):
        self.news.save()
        self.news_kedua.save()
        filtered_status = News.objects.filter(
            status__startswith="2").get().status
        filtered_news_title = News.objects.filter(
            status__startswith="2").get().title
        self.assertEqual(self.news_status_publish, filtered_status)
        self.assertEqual(self.news_name_2, filtered_news_title)

    def test_model_filter_news_by_topic_title(self):
        self.news.save()
        self.news_kedua.save()
        self.topic.save()
        self.topic_kedua.save()
        self.news.topics.add(self.topic, self.topic_kedua)
        self.news_kedua.topics.add(self.topic)
        filtered_news = News.objects.filter(topics__name__startswith="Ferrari")
        self.assertEqual(self.news.title, filtered_news.get().title)


class ViewTestCase(TestCase):

    def setUp(self):
        self.user = User.objects.create_superuser(
            username="admin", email="admin@admin.com", password="admin.password")
        self.client = APIClient()
        self.client.force_authenticate(user=self.user)

        self.news_data = {
            'title': 'Pembukaan Kampus baru di Jogja',
            'content': 'ini content',
            'status': '0',
            'author': self.user.id
        }
        self.response = self.client.post(
            reverse('create'),
            self.news_data,
            format="json"
        )
        self.topic_data = {
            'name': 'Yogyakarta',
            'author': self.user.id
        }
        self.response_topic = self.client.post(
            reverse('create-topic'),
            self.topic_data,
            format="json"
        )

    def test_api_create_news(self):
        self.assertEqual(self.response.status_code, status.HTTP_201_CREATED)

    def test_api_create_topic(self):
        self.assertEqual(
            self.response_topic.status_code,
            status.HTTP_201_CREATED)

    def test_authorization_is_enforced(self):
        new_client = APIClient()
        response = new_client.get('/news/', kwargs={'pk': 3}, format="json")
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)

    def test_api_get_news(self):
        news = News.objects.get(id=1)
        response = self.client.get(
            '/news/',
            kwargs={'pk': news.id},
            format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, news)

    def test_api_get_topic(self):
        topic = Topic.objects.get(id=1)
        response = self.client.get(
            '/topic/',
            kwargs={'pk': topic.id},
            format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertContains(response, topic)

    def test_api_update_news(self):
        news = News.objects.get()
        update_news = {
            'title': 'Film Bayi Gaib rilis',
            'content': 'Hari ini film yang dibintangi Mas Bram akan rilis',
            'status': '2'
        }
        response = self.client.put(
            reverse('details', kwargs={
                'pk': news.id
            }),
            update_news,
            format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['title'], update_news['title'])

    def test_api_assign_topic_into_news(self):
        news = News.objects.get()
        topic = Topic.objects.get()
        update_data = {
            'title': news.title,
            'content': news.content,
            'status': news.status,
            'topics': [topic.id]
        }
        response = self.client.put(
            reverse('details', kwargs={
                'pk': news.id
            }),
            update_data,
            format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['topics'], [topic.id])

    def test_api_update_topic(self):
        topic = Topic.objects.get()
        update_topic = {
            'name': 'Pemprov Yogyakarta akan Mengadakan Event Akbar'
        }
        response = self.client.put(
            reverse('details-topic', kwargs={
                'pk': topic.id
            }),
            update_topic,
            format="json",
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data['name'], update_topic['name'])

    def test_api_filter_news_by_status(self):
        news = News.objects.get()
        status_published = '0'
        response = self.client.get(
            '/news/',
            {'status=': status_published},
            format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()[0]['status'], status_published)

    def test_api_filter_news_by_topic_name(self):
        news = News.objects.get()
        topic = Topic.objects.get()
        update_data = {
            'title': news.title,
            'content': news.content,
            'status': news.status,
            'topics': [topic.id]
        }
        self.client.put(
            reverse('details', kwargs={
                'pk': news.id
            }),
            update_data,
            format="json"
        )
        response = self.client.get(
            '/news/',
            {'topics': topic.name},
            format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()[0]['topics'], [topic.id])

    def test_api_filter_news_by_title(self):
        news = News.objects.get()
        title = "Jogja"
        response = self.client.get(
            '/news/',
            {'q': title},
            format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.json()[0]['title'], news.title)

    def test_api_delete_news(self):
        news = News.objects.get()
        old_count = News.objects.count()
        response = self.client.delete(
            reverse('details', kwargs={
                'pk': news.id
            }),
            format="json",
            follow=True
        )
        new_count = News.objects.count()
        self.assertNotEqual(old_count, new_count)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_api_delete_topic(self):
        topic = Topic.objects.get()
        old_count = Topic.objects.count()
        response = self.client.delete(
            reverse('details-topic', kwargs={
                'pk': topic.id
            }),
            format="json",
            follow=True
        )
        new_count = Topic.objects.count()
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertNotEqual(old_count, new_count)

    def test_api_get_token(self):
        self.user_data = {
            'username': 'admin',
            'password': 'admin.password'
        }
        self.response = self.client.post(
            reverse('get-token'),
            self.user_data,
            format="json"
        )
        self.assertEqual(self.response.status_code, status.HTTP_200_OK)
        self.assertContains(self.response, "token")
