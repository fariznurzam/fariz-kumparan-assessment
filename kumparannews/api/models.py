from django.db import models
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from django.dispatch import receiver
from .utils import DBCurrentTimestamp


class Topic(models.Model):
    name = models.CharField(max_length=255, blank=False, unique=True)
    description = models.TextField(null=True)
    author = models.ForeignKey(
        'auth.User', related_name='topic', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ('name',)


class News(models.Model):
    STATUSCHOICE = (
        ('0', 'draft'),
        ('1', 'deleted'),
        ('2', 'publish'),
    )
    title = models.CharField(max_length=255, blank=False, unique=True)
    content = models.TextField(blank=False)
    status = models.CharField(max_length=1, choices=STATUSCHOICE)
    topics = models.ManyToManyField(Topic, blank="True")
    author = models.ForeignKey(
        'auth.User', related_name='news', on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('title',)


@receiver(post_save, sender=User)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)


DBCurrentTimestamp.patch(
    models.DateField,
    models.TimeField,
    models.DateTimeField)
