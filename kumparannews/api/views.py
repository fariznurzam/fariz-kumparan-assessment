import django_filters
from django.shortcuts import render
from rest_framework import generics
from .serializers import NewsSerializer, TopicSerializer
from .models import News, Topic
from django.contrib.auth.models import User
from rest_framework import permissions
from .permissions import IsAuthor
from django.db.models import Q
from rest_framework.filters import SearchFilter, OrderingFilter


class CreateView(generics.ListCreateAPIView):
    serializer_class = NewsSerializer
    permission_classes = (permissions.IsAuthenticated, IsAuthor)
    filter_backends = [SearchFilter, OrderingFilter]
    search_fields = ('topics__name', 'status')
    ordering_fields = ('title', 'status', 'author', 'created_at')
    ordering = ('title')

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    def get_queryset(self, *args, **kwargs):
        queryset_list = News.objects.all()

        status = self.request.query_params.get('status', None)
        topics = self.request.query_params.get('topics', None)
        query = self.request.GET.get('q')
        if status is not None:
            queryset_list = queryset_list.filter(
                status=status
            )
        elif topics is not None:
            queryset_list = queryset_list.filter(
                topics__name=topics
            )
        elif query:
            queryset_list = queryset_list.filter(
                Q(title__icontains=query) |
                Q(status__icontains=query) |
                Q(topics__name__icontains=query)
            ).distinct()
        return queryset_list


class DetailsView(generics.RetrieveUpdateDestroyAPIView):
    queryset = News.objects.all()
    serializer_class = NewsSerializer
    permission_classes = (permissions.IsAuthenticated, IsAuthor)


class TopicCreateView(generics.ListCreateAPIView):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer
    permission_classes = (permissions.IsAuthenticated, IsAuthor)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)


class TopicDetailsView(generics.RetrieveUpdateDestroyAPIView):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer
    permission_classes = (permissions.IsAuthenticated, IsAuthor)
